package com.week3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class QuickSort {

	/**
	 * @param args
	 */
	private int[] input;
	private int numOfComparisons;
	
	public QuickSort(FileReader file, int inputSize)throws IOException{
		
		BufferedReader reader = new BufferedReader(file);
		String line;
		input = new int[inputSize];
		int idx = 0;
		while((line = reader.readLine()) != null){
			
			input[idx] = Integer.parseInt(line.trim());
			idx++;
		}
		
	}
	
	public int sortAndgetComparisonsCount(){
		
		quickSorting(input, 0, input.length - 1);
		return numOfComparisons;
		
	}
	private void quickSorting(int[] inputArray, int left, int right){
		
   		if(left == right){
			return;
		}else{
			numOfComparisons += right - left;
			int pivotFinalPosition = partition(inputArray, left, right);
			
			if(pivotFinalPosition > left){
			quickSorting(inputArray, left, pivotFinalPosition - 1);
			}
			if(pivotFinalPosition < right){
			quickSorting(inputArray, pivotFinalPosition + 1, right);
			}
			
		}
	}
	
	private int partition(int[] inputArray, int left, int right){
		// return the pivot position
		// below line has to be included if the pivot chosen should be the last element of the array
		//swap(left,right,inputArray);
		int pivot = getMedian(inputArray, left, right);
		//int pivot = inputArray[left];
		int i = left + 1;
		
		for(int j = left + 1; j <= right; j++){
			
			if(inputArray[j] < pivot){
				swap(j,i,inputArray);
				i++;
			}
		}
		swap(left,i-1,inputArray);
		
		return i-1;
	}
	
	private void swap(int position1, int position2, int[] arr){
		
		if(position1 == position2){
			return;
		}
		int temp = arr[position1];
		arr[position1] = arr[position2];
		arr[position2] = temp;
	}
	
	private int getMedian(int[] arr, int left, int right){
		
		int midIndex = left + ((right-left)/2);
		int first = arr[left];
		int last = arr[right];
		int middle = arr[midIndex]; // BUG FOUND
		
		if((middle >= first && middle <= last) || (middle >= last && middle <= first)){
			swap(midIndex,left,arr);
			return middle;
		}else if((first >= middle && first <= last) || (first <= middle && first >= last)){
			return first;
		}else{
			swap(right,left,arr);
			return last;
		}
		
	}
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		FileReader file = new FileReader("/Users/spoor/Desktop/Algorithms_assignment/Algorithms1/week3/QuickSort.txt");
		
		QuickSort qs = new QuickSort(file, 10000);
		System.out.println(qs.sortAndgetComparisonsCount());
	}

}
