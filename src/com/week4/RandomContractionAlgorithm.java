package com.week4;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;


public class RandomContractionAlgorithm {

	
	private HashMap<Integer, ArrayList<Integer>> adjascencyList;

	private int[] parent;



	public RandomContractionAlgorithm(FileReader file) throws IOException{

		BufferedReader reader = new BufferedReader(file);
		String line;

		adjascencyList = new HashMap<Integer, ArrayList<Integer>>();



		while((line = reader.readLine()) != null){

			String[] row = line.trim().split("	");
			int vertex = Integer.parseInt(row[0]);
			ArrayList<Integer> adjVertieces = new ArrayList<Integer>();
			for(int i = 1; i < row.length; i++){
				int adjVertex = Integer.parseInt(row[i]);
				adjVertieces.add(adjVertex);



			}

			adjascencyList.put(vertex, adjVertieces);
		}

		parent = new int[adjascencyList.size() + 1];



	}


	public int minCostCut(){

		for(int i = 1; i <= adjascencyList.size(); i++){
			parent[i] = i;
		}
		
		HashMap<Integer,ArrayList<Integer>> copyList = new HashMap<Integer,ArrayList<Integer>>();
		for(Map.Entry<Integer, ArrayList<Integer>> original: adjascencyList.entrySet()){ // because we don't need shallow copy here.
			copyList.put(original.getKey(), new ArrayList<Integer>(original.getValue()));
		}

		Random rand = new Random();
		
		while(copyList.size() > 2){

			int vertex1 = 0;
			int vertex2 = 0;
			int vertex2Idx = 0;

			while(vertex1 == vertex2 || !copyList.containsKey(vertex1) || !copyList.containsKey(vertex2)){
				vertex1 = rand.nextInt(adjascencyList.size()) + 1;
				if(copyList.containsKey(vertex1)){
					ArrayList<Integer> otherEnds = copyList.get(vertex1);

					vertex2Idx = rand.nextInt(otherEnds.size());
					vertex2 = otherEnds.get(vertex2Idx);
				}
			}
            copyList = union(vertex1, vertex2, copyList);

		}


		int minCutCost = 0;
		Iterator mapIterator = copyList.entrySet().iterator();
		Map.Entry vertexFirst = (Map.Entry) mapIterator.next();
		Integer first = (Integer) vertexFirst.getKey();
		Map.Entry vertexSecond = (Map.Entry) mapIterator.next();
		Integer second = (Integer) vertexSecond.getKey();

		
		ArrayList<Integer> commonAdjNodes = copyList.get(first);
		for(Integer adjNode: commonAdjNodes){
			if( findParent(adjNode) == second ){
				minCutCost++;
			}
		}
		mapIterator.remove();
        return minCutCost;
	}


	private int findParent(int vertex){

		int parentVertex = vertex;
		while(parent[parentVertex] != parentVertex){
			parentVertex = parent[parentVertex];
		}
		return parentVertex;
	}

	private HashMap<Integer,ArrayList<Integer>> union(int node1, int node2,HashMap<Integer,ArrayList<Integer>> copyList){


		parent[node2] = node1;

		ArrayList<Integer> adjVerticesOfSecondVertex = copyList.get(node2);
		for(Integer eachVertex : adjVerticesOfSecondVertex){
			if( findParent(eachVertex) != node1 && copyList.containsKey(findParent(eachVertex))){ // avoiding self-loops check this!!

				copyList.get(node1).add(findParent(eachVertex));
			}

		}


		copyList.remove(node2); // delete the vertex merged
		return copyList;
	}

	public static void main(String[] args)throws IOException {

		FileReader file = new FileReader("/Users/spoor/Desktop/Algorithms_assignment/Algorithms1/week4/kargerMinCut.txt");

		RandomContractionAlgorithm randomContraction = new RandomContractionAlgorithm(file);
		int minimumEdgesAfterCut = Integer.MAX_VALUE;
		
		for(int numOfTrials = 0; numOfTrials < 40000; numOfTrials++){
			int minCost = randomContraction.minCostCut();

			//System.out.println("Trial " +numOfTrials+ ", cost: " +minCost);
			if(minCost < minimumEdgesAfterCut){
				minimumEdgesAfterCut = minCost;
			}
		}


		System.out.println("Final min cut : " +minimumEdgesAfterCut);
		// For kargerMinCut.txt, the solution is 17. 
		// For this file alone, in the initial reading of file, split every line by a tab and not space
		//( space has to be used for all other test cases).
	}

}
