package com.week2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class CountInversions {

	
	
	private int[] input;
	private long count;
	
	public CountInversions(FileReader file, int numberOfIntegers) throws IOException{
		
		BufferedReader reader = new BufferedReader(file);
		String line;
		
		count = 0;
		input = new int[numberOfIntegers];
		int index = 0;
		while((line = reader.readLine()) != null){
			input[index] = Integer.parseInt(line.trim());
			index++;
		}
	}
	
	public void numOFInversions(){
		sortAndDivide(input, input.length);
		System.out.println("Total number of inversions = " +count);
	}
	private void sortAndDivide(int[] givenArray, int length){
		
		if(length == 1){
			return ;
		}else{
			int mid = length/2;
			// int rightLength = length/2;
			if(length % 2 != 0){ //odd length array
				mid = length/2 + 1;
			}
			int[] leftHalf = Arrays.copyOfRange(givenArray, 0, mid);
			int[] rightHalf = Arrays.copyOfRange(givenArray, mid, length);
			sortAndDivide(leftHalf, leftHalf.length);
			sortAndDivide(rightHalf, rightHalf.length);
			countSplitAndMerge(givenArray, leftHalf, rightHalf);
			
		}
	}
	
	private void countSplitAndMerge(int[] givenArray, int[] leftHalf, int[] rightHalf){
		
		int k = 0;
		int i = 0;
		int j = 0;
		//int count = 0;
		while(k < givenArray.length){
			
			if(leftHalf[i] < rightHalf[j]){
				givenArray[k] = leftHalf[i];
				if(i == leftHalf.length - 1){
					
					for(int idx = k+1; idx < givenArray.length; idx++){
						givenArray[idx] = rightHalf[j];
						j++;
					}
					break;
				}
				i++;
			}else{ // leftHalf[i] > rightHalf[j]
				
				givenArray[k] = rightHalf[j];
				count += leftHalf.length - i;
				if(j == rightHalf.length - 1){
					for(int idx = k+1; idx < givenArray.length; idx++){
						givenArray[idx] = leftHalf[i];
						i++;
					}
					break;
				}
				j++;
			}
			k++;
		}
		
	}
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		FileReader file = new FileReader("/Users/spoor/Desktop/Algorithms_assignment/Algorithms1/week2/IntegerArray.txt");
		CountInversions countInversion = new CountInversions(file,100000);
		countInversion.numOFInversions();

		// Ans - 2407905288
	}

}
