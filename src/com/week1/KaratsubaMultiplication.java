package com.week1;

import java.math.BigDecimal;
import java.math.BigInteger;

public class KaratsubaMultiplication {

	/**
	 * @param args
	 */
	private String numberX;
	private String numberY;
	
	
	public KaratsubaMultiplication(String number1,String number2){
		
		
		numberX = number1;
		numberY = number2;
		
	}
	
	public BigInteger printTheSolution(){
		BigInteger solution = multiply(numberX,numberY);
		
		System.out.println("Solution is " + solution);
		return solution;
	}
	
	private BigInteger multiply(String x,String y){
		
		if(new BigInteger(x).equals(new BigInteger("0")) || new BigInteger(y).equals(new BigInteger("0"))){
			return new BigInteger("0");
		}
		
		int xn = x.length();
		int yn = y.length();
		if(x.length() <=2 || y.length() <= 2){
			return new BigInteger(x).multiply(new BigInteger(y));
		}
		if(x.length() != y.length()){
			if(x.length() > y.length()){
				//int lengthDiff = x.length() - y.length();
				y = String.format("%1$"+ x.length() + "s",y).replace(' ', '0');
			}else{
				// int lengthDiff = y.length() - x.length();
				x = String.format("%1$"+ y.length() + "s", x).replace(" ", "0");
			}
		}
		
		int n = x.length();
		
		
		// todo : correct this abs thing 
		String a = x.substring(0, (int)Math.ceil(n/2.0));
		String b = x.substring((int)Math.ceil(n/2.0),x.length());
		
		String c = y.substring(0, (int)Math.ceil(n/2.0));
		String d = y.substring((int)Math.ceil(n/2.0),y.length());
		
		BigInteger ac, bd, aPlusbMulcPlusd;
		if(a.length() <= 2 && c.length() <= 2){
			ac = new BigInteger(a).multiply(new BigInteger(c));
		}else{
			ac = multiply(a, c);
		}
		
		if(b.length() <= 2 && d.length() <= 2){
			bd = new BigInteger(b).multiply(new BigInteger(d));
		}else{
			bd = multiply(b, d);
		}
		
		String aPlusb = new BigInteger(a).add(new BigInteger(b)).toString();
		String cPlusd = new BigInteger(c).add(new BigInteger(d)).toString();
		
		if(aPlusb.length() <= 2 && cPlusd.length() <= 2){
             aPlusbMulcPlusd = new BigInteger(aPlusb).multiply(new BigInteger(cPlusd));
		}else{
			aPlusbMulcPlusd = multiply(aPlusb, cPlusd);
		}
		
		BigInteger secPart = aPlusbMulcPlusd.subtract(ac).subtract(bd);
		
        if(x.length() % 2 != 0){ // n has to change if the string lengths are odd
			
			n = x.length() - 1;
		}else{
			n = x.length();
		}
		BigInteger firstPart = new BigInteger(new BigDecimal(Math.pow(10, n)).toString()).multiply(ac);
		BigInteger fullSecPart = new BigInteger(new BigDecimal(Math.pow(10, n/2)).toString()).multiply(secPart);
		
		return firstPart.add(fullSecPart).add(bd);
		
	}
	
	public static void main(String[] args) {
		
		
		KaratsubaMultiplication km = new KaratsubaMultiplication("3000000000000000000000000", "5000000000000000000000");
		// KaratsubaMultiplication km = new KaratsubaMultiplication("3141592653589793238462643383279502884197169399375105820974944592", "2718281828459045235360287471352662497757247093699959574966967627");
		BigInteger y = km.printTheSolution();
		
		BigInteger x = new BigInteger("3000000000000000000000000").multiply(new BigInteger("5000000000000000000000"));
		//BigInteger x = new BigInteger("3141592653589793238462643383279502884197169399375105820974944592").multiply(new BigInteger("2718281828459045235360287471352662497757247093699959574966967627"));
		System.out.println("Big : " +x);
		if(x.equals(y)){
			System.out.println("yesssss correct");
		}
		//System.out.println("BigI: " +new BigInteger("125897598").multiply(new BigInteger("789876765")));

	}

}
